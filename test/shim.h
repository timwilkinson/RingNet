#include <stdio.h>
#include <inttypes.h>
#include <stddef.h>
#include <memory.h>

#define SHIM 1

#define private public

#define PROGMEM

class ShimSerial
{
private:
  uint8_t txbuf[32];
  uint8_t txlen;
  uint8_t rxbuf[32];
  uint8_t rxlen;

public:
  void begin(uint32_t baud, uint8_t config);
  int16_t available(void);
  int16_t availableForWrite(void);
  size_t write(const uint8_t v);
  int16_t read(void);
  void sendTo(ShimSerial& target);
};

class ShimPrint
{
public:
  virtual size_t write(const uint8_t* buffer, size_t length) = 0;
  void print(const char* msg);
};

#define Print ShimPrint

#define HardwareSerial  ShimSerial
#define	SERIAL_8N1 0

extern void delayMicroseconds(uint16_t nr);
extern uint16_t millis(void);
extern uint8_t pgm_read_byte_near(const uint8_t* addr);
