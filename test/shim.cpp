#include <stdio.h>
#include <assert.h>
#include <sys/time.h>
#include "shim.h"
#include "../RingNet.h"

#define NR_NODES  8

static struct
{
  ShimSerial serial;
  RingNet* net;
} nodes[NR_NODES];
static RingNet* current;

static void init(void);
static void initMaster(RingNet* net);
static void initSlave(RingNet* net, int idx);
static void step(void);
static bool idle(void);
static void loop(void);
static void loopMaster(RingNet* net);
static void loopSlave(RingNet* net);


int main(int argc, char* argv[])
{
  init();
  for (int l = 0; !idle() && l < 500; l++)
  {
    printf("%4d: ", l);
    for (int i = 0; i < NR_NODES; i++)
    {
      printf("%d ", nodes[i].net->state);
    }
    printf("\n");

    loop();
  }
}

// --- Shim Serial ---

void ShimSerial::begin(uint32_t baud, uint8_t config)
{
}

int16_t ShimSerial::available(void)
{
  return rxlen;
}

int16_t ShimSerial::availableForWrite(void)
{
  return sizeof(txbuf) - txlen;
}

size_t ShimSerial::write(const uint8_t v)
{
  assert (txlen < sizeof(txbuf));
  txbuf[txlen++] = v;
  return 1;
}

int16_t ShimSerial::read(void)
{
  assert(rxlen > 0);
  uint8_t c = rxbuf[0];
  rxlen--;
  memmove(&rxbuf[0], &rxbuf[1], rxlen);
  return c;
}

void ShimSerial::sendTo(ShimSerial& target)
{
  if (txlen > 0)
  {
    assert(target.rxlen < sizeof(target.rxbuf));
    target.rxbuf[target.rxlen++] = txbuf[0];
    txlen--;
    memmove(&txbuf[0], &txbuf[1], txlen);
  }
}

// ---

void ShimPrint::print(const char* msg)
{
  write((uint8_t*)msg, strlen(msg));
}

// ---

uint8_t pgm_read_byte_near(const uint8_t* addr)
{
  return *addr;
}

void delayMicroseconds(uint16_t delay)
{
  step();
}

uint16_t millis(void)
{
  struct timeval time;
  gettimeofday(&time, NULL);
  return (uint16_t)(time.tv_sec * 1000 + time.tv_usec / 1000);
}

// ---

static void init(void)
{
  for (int i = 0; i < NR_NODES; i++)
  {
    nodes[i].net = new RingNet(nodes[i].serial);
    if (i == 0)
    {
      initMaster(nodes[i].net);
    }
    else
    {
      initSlave(nodes[i].net, i);
    }
  }
}

static void step(void)
{
  // Step data between the nodes
  for (int i = NR_NODES - 1; i >= 0; i--)
  {
    nodes[i].serial.sendTo(nodes[(i + 1) % NR_NODES].serial);
  }
}

static bool idle(void)
{
  for (int i = 0; i < NR_NODES; i++)
  {
    if (nodes[i].serial.txlen > 0 || nodes[i].serial.rxlen > 0)
    {
      return false;
    }
    if (nodes[i].net->state > RingNet::STATE_DST)
    {
      return false;
    }
  }
  return true;
}

static void loop(void)
{
  step();

  // Execute each node in turn
  for (int i = 0; i < NR_NODES; i++)
  {
    current = nodes[i].net;
    if (i == 0)
    {
      loopMaster(nodes[i].net);
    }
    else
    {
      loopSlave(nodes[i].net);
    }
  }
}

// ---

static void status(const RingNet::StatusCode err)
{
  if (err == RingNet::ERROR_REMOVE)
  {
    printf("-- REMOVE\n");
    return;
  }
  if (err == RingNet::ERROR_REMOVED)
  {
    printf("-- REMOVED\n");
    return;
  }
  if (err == RingNet::ACK)
  {
    printf("-- ACK\n");
    return;
  }
  printf("Error %d\n", err);
  assert(err == 0);
}

static void recv(const uint8_t from, const uint8_t* data, uint8_t length)
{
  printf("from %d to %d [", from, current->localAddress);
  for (int i = 0; i < length; i++)
  {
    printf(" %02x", data[i]);
  }
  printf(" ]\n");

  if (current->localAddress != RingNet::ADDRESS_MASTER)
  {
    const uint8_t reply[] = { 0 };
    int r = current->reply(reply, sizeof(reply));
    assert(r == 0);
    current->debug()->print("HELLO");
  }
}

static void debug(const uint8_t from, const char* msg)
{
  printf("-- MSG: %d: %s\n", from, msg);
}

static void resolved(const uint8_t from, const uint8_t address[RingNet::ADDRESS_LENGTH])
{
  printf("-- RESOLVED: %d: %d\n", from, address[7]);
}

static void reset()
{
  printf("-- RESET\n");
  const uint8_t msg[] = { 1, 2, 3, 4 };
  int r = -1;
  //r = current->send(RingNet::ADDRESS_FIRST + 3, msg, sizeof(msg));
  //r = current->send(RingNet::ADDRESS_FIRST + 2, msg, sizeof(msg));
  r = current->list();
  assert(r == 0);
}

static void initMaster(RingNet* net)
{
  const uint8_t master[] = { 0, 0, 0, 0, 0, 0, 0, 1 };
  net->setAddress(master);
  net->setOnReceive(recv);
  net->setOnStatus(status);
  net->setOnDebug(debug);
  net->setOnReset(reset);
  net->setOnResolved(resolved);
  int r = net->reset();
  assert(r == 0);
}

static void initSlave(RingNet* net, int idx)
{
  const uint8_t slave[] = { 0, 0, 0, 0, 0, 0, 0, 1 + idx };
  net->setAddress(slave);
  net->setOnReceive(recv);
  net->setOnStatus(status);
}

static void loopMaster(RingNet* net)
{
  net->loop();
}

static void loopSlave(RingNet* net)
{
  net->loop();
}
