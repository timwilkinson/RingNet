#include <inttypes.h>

class HardwareSerial;

class RingNet
{
  //
  // Packet format:
  //  [ SYNC:8, MASTERED:1, DST_ADDRESS:7, SRC_ADDRESS:8, PROTOCOL:8, DATA:8N, CRC:8, SYNC:8 ]
  //
  public:
    enum StatusCode
    {
      OK = 0,
      ACK = 1,
      NACK = 2,
      ERROR_CRC = 3,
      ERROR_TOOBIG = 4,
      ERROR_REMOVE = 5,
      ERROR_REMOVED = 6,
      ERROR_EMPTY = 7,
      ERROR_BUSY = 8,
      ERROR_SPACE = 9,
      ERROR_BADDATA = 10,
      ERROR_INITIALIZING = 11,
      ERROR_ADDRESS = 12,
      ERROR_PROTOCOL = 13,
      ERROR_PENDING = 14
    };
    
    const static uint8_t ADDRESS_LENGTH = 8;
    
    typedef void (*OnReceive)(const uint8_t from, const uint8_t* data, uint8_t length);
    typedef void (*OnReady)(void);
    typedef void (*OnDebug)(const uint8_t from, const char* msg);
    typedef void (*OnStatus)(const StatusCode error);
    typedef void (*OnReset)(void);
    typedef void (*OnResolved)(const uint8_t shortAddress, const uint8_t longAddress[ADDRESS_LENGTH]);
    typedef void (*OnAddress)(const uint8_t longAddress[ADDRESS_LENGTH]);
    typedef void (*OnNetworkChange)(const uint8_t from);

    RingNet(HardwareSerial& serial);

    void setOnAddress(OnAddress address);
    void setOnReceive(OnReceive recv);
    void setOnReady(OnReady ready);
    void setOnReset(OnReset reset);
    void setOnStatus(OnStatus status);
    void setOnResolved(OnResolved resolved);
    void setOnNetworkChange(OnNetworkChange change);
    void setOnDebug(OnDebug debug);
  
    StatusCode enableDebug(const uint8_t remoteAddress, const bool enable = true);
    void setAddress(const uint8_t address[ADDRESS_LENGTH]);

    void begin(const uint32_t baud = 9600);
    StatusCode reset(void);
    StatusCode reply(const uint8_t* data, const uint8_t length, const bool ack = true);
    StatusCode send(const uint8_t remoteAddress, const uint8_t* data, const uint8_t length, const bool ack = true);
    StatusCode resolve(const uint8_t longAddress[8]);
    StatusCode list(void);
    StatusCode networkChange(void);
    uint8_t nodeCount(void);
    void loop(void);
    bool available(void);
    Print* debug(void);

  private:
    enum State
    {
      STATE_SYNC = 0,
      STATE_DST,
      STATE_SRC,
      STATE_PROTOCOL,
      STATE_CRC,
      STATE_DATA,
      STATE_ESCDATA,
      STATE_SRCFORWARD,
      STATE_DATAFORWARD,
    };

    enum Flags
    {
      FLAG_DEBUG_ENABLED  = 0x01,
      FLAG_IN_RESET       = 0x02,
    };

    enum Pkt
    {
      PKT_SYNC         = 0xF5,
      PKT_ESC          = 0xF6,
      PKT_DST_MASK     = 0x7F,
      PKT_DST_MASTERED = 0x80
    };

    enum Address
    {
      ADDRESS_NULL    = 0x00,
      ADDRESS_MASTER  = 0x01,
      ADDRESS_FIRST   = 0x02,
      ADDRESS_LAST    = 0x7E,
      ADDRESS_NEXT    = 0x7F
    };

    enum Command
    {
      COMMAND_RESET          = 0x01,
      COMMAND_RESET_CONFIRM  = 0x02,
      COMMAND_RESOLVE        = 0x03,
      COMMAND_RESOLVED       = 0x04,
      COMMAND_LIST           = 0x05,
      COMMAND_SET_ADDRESS    = 0x06,
      COMMAND_ACK            = 0x10,
      COMMAND_NACK           = 0x11,
      COMMAND_NETWORK_CHANGE = 0x12,
      COMMAND_DEBUG          = 0x20,
      COMMAND_DEBUG_ENABLE   = 0x21,
    };

    enum Protocol
    {
      PROTOCOL_LEVEL_BIT  = 0x10,
      PROTOCOL_SYSTEM     = 0x00,
      PROTOCOL_USER       = 0x10,
      PROTOCOL_ACK_BIT    = 0x40
    };

    class DebugPrint : Print
    {
    private:
      RingNet* net;
    public:
      DebugPrint(RingNet* net);
      size_t write(uint8_t ch);
      size_t write(const uint8_t* buffer, size_t len);
      friend class RingNet;
    };

    const static uint16_t RINGNET_TIMEOUT = 1000; // ms
    const static uint16_t RINGNET_RESET_TIMEOUT = 5000; // ms
    const static uint8_t  RINGNET_SERIAL_CONFIG = SERIAL_8N1;
    const static uint8_t RINGNET_MAX_DATA = 16;

    StatusCode sendInternal(const uint8_t localAddress, const uint8_t remoteAddress, uint8_t protocol, const uint8_t* data, const uint8_t length, const bool ack);
    StatusCode sendInternalRetry(const uint8_t localAddress, const uint8_t remoteAddress, uint8_t protocol, const uint8_t* data, const uint8_t length, const bool ack);
    void error(StatusCode err);
    void ready(void);
    void recv(void);
    void ack(bool okay);
    
  private:
    uint8_t flags;
    State state;
    HardwareSerial* serial;
    OnAddress addrFn;
    OnReceive recvFn;
    OnReady readyFn;
    OnStatus statusFn;
    OnReset resetFn;
    OnResolved resolvedFn;
    OnNetworkChange changeFn;
    OnDebug debugFn;
    uint8_t localLongAddress[ADDRESS_LENGTH];
    uint8_t localAddress;
    uint8_t remoteAddress;
    uint8_t forwardAddress;
    uint8_t remoteProtocol;
    uint8_t nrNodes;
    uint8_t buffer[RINGNET_MAX_DATA];
    uint8_t* bufferPtr;
    uint16_t lastTime;
    uint16_t loopTime;
    struct
    {
      uint16_t time;
      uint8_t address;
      uint8_t protocol;
      const uint8_t* data;
      uint8_t length;
    }       pending;
};
