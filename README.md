# RingNet
## Simple network of Arduinos built using a ring of serial connections

The RingNet library makes it simple to connect a large number of Arduinos together in a network.
Arduino UNOs and Megas do not come with any networking hardware, but there is sometimes a project needs
to connect many such devices together. Rather than adding cost, this library lets many
board communicate easily by connecting their serial lines together in a loop; the transmit of each board
connects to the received of the next board, with the final connection looping back to the first.

A typical setup of 4 Arduino's might look like this:

```
              | Uno |             | Uno |            | Uno |
              |     |             |     |            |     |
              |RX TX|             |RX TX|            |RX TX|
              +-|-|-+             +-|-|-+            +-|-|-+
----+           | |                 | |                | |
M  TX-----------/ \-----------------/ \----------------/ |
e   |                                                    |
g  RX----------------------------------------------------/
a   |
----+
```

The library provides reliable, ordered, at-least-once, datagram delivery. Other protocols can easily be
built on top of this if required. For simplicity, a debugging 'serial' service is provided by default which
operates just like the standard Serial.print system. This is particulary useful when debugging multiple boards.
