#pragma GCC optimize ("O3")

#if defined(ARDUINO)
#include <Arduino.h>
#include <avr/pgmspace.h>
#elif defined(__APPLE__)
#include "test/shim.h"
#endif
#include "RingNet.h"

static const uint8_t PROGMEM crcTable[] =
{
    0x00, 0x07, 0x0e, 0x09, 0x1c, 0x1b, 0x12, 0x15, 0x38, 0x3f, 0x36, 0x31,
    0x24, 0x23, 0x2a, 0x2d, 0x70, 0x77, 0x7e, 0x79, 0x6c, 0x6b, 0x62, 0x65,
    0x48, 0x4f, 0x46, 0x41, 0x54, 0x53, 0x5a, 0x5d, 0xe0, 0xe7, 0xee, 0xe9,
    0xfc, 0xfb, 0xf2, 0xf5, 0xd8, 0xdf, 0xd6, 0xd1, 0xc4, 0xc3, 0xca, 0xcd,
    0x90, 0x97, 0x9e, 0x99, 0x8c, 0x8b, 0x82, 0x85, 0xa8, 0xaf, 0xa6, 0xa1,
    0xb4, 0xb3, 0xba, 0xbd, 0xc7, 0xc0, 0xc9, 0xce, 0xdb, 0xdc, 0xd5, 0xd2,
    0xff, 0xf8, 0xf1, 0xf6, 0xe3, 0xe4, 0xed, 0xea, 0xb7, 0xb0, 0xb9, 0xbe,
    0xab, 0xac, 0xa5, 0xa2, 0x8f, 0x88, 0x81, 0x86, 0x93, 0x94, 0x9d, 0x9a,
    0x27, 0x20, 0x29, 0x2e, 0x3b, 0x3c, 0x35, 0x32, 0x1f, 0x18, 0x11, 0x16,
    0x03, 0x04, 0x0d, 0x0a, 0x57, 0x50, 0x59, 0x5e, 0x4b, 0x4c, 0x45, 0x42,
    0x6f, 0x68, 0x61, 0x66, 0x73, 0x74, 0x7d, 0x7a, 0x89, 0x8e, 0x87, 0x80,
    0x95, 0x92, 0x9b, 0x9c, 0xb1, 0xb6, 0xbf, 0xb8, 0xad, 0xaa, 0xa3, 0xa4,
    0xf9, 0xfe, 0xf7, 0xf0, 0xe5, 0xe2, 0xeb, 0xec, 0xc1, 0xc6, 0xcf, 0xc8,
    0xdd, 0xda, 0xd3, 0xd4, 0x69, 0x6e, 0x67, 0x60, 0x75, 0x72, 0x7b, 0x7c,
    0x51, 0x56, 0x5f, 0x58, 0x4d, 0x4a, 0x43, 0x44, 0x19, 0x1e, 0x17, 0x10,
    0x05, 0x02, 0x0b, 0x0c, 0x21, 0x26, 0x2f, 0x28, 0x3d, 0x3a, 0x33, 0x34,
    0x4e, 0x49, 0x40, 0x47, 0x52, 0x55, 0x5c, 0x5b, 0x76, 0x71, 0x78, 0x7f,
    0x6a, 0x6d, 0x64, 0x63, 0x3e, 0x39, 0x30, 0x37, 0x22, 0x25, 0x2c, 0x2b,
    0x06, 0x01, 0x08, 0x0f, 0x1a, 0x1d, 0x14, 0x13, 0xae, 0xa9, 0xa0, 0xa7,
    0xb2, 0xb5, 0xbc, 0xbb, 0x96, 0x91, 0x98, 0x9f, 0x8a, 0x8d, 0x84, 0x83,
    0xde, 0xd9, 0xd0, 0xd7, 0xc2, 0xc5, 0xcc, 0xcb, 0xe6, 0xe1, 0xe8, 0xef,
    0xfa, 0xfd, 0xf4, 0xf3
};
#define CRC8(CRC, V) (CRC = pgm_read_byte_near(&crcTable[(CRC) ^ (V)]))

RingNet::RingNet(HardwareSerial& serial)
{
  this->flags = 0;
  this->serial = &serial;
  this->addrFn = NULL;
  this->recvFn = NULL;
  this->readyFn = NULL;
  this->statusFn = NULL;
  this->resetFn = NULL;
  this->resolvedFn = NULL;
  this->debugFn = NULL;
  this->nrNodes = 0;
  this->localAddress = ADDRESS_NULL;
  this->remoteAddress = ADDRESS_NULL;
  this->forwardAddress = ADDRESS_NULL;
  memset(this->localLongAddress, 0, ADDRESS_LENGTH);
  this->remoteProtocol = 0;
  this->bufferPtr = this->buffer;
  this->lastTime = millis();
  this->pending.time = 0;
  this->pending.address = ADDRESS_NULL;
  this->pending.protocol = 0;
  this->pending.data = NULL;
  this->pending.length = 0;
  this->loopTime = 0xFFFF;
  this->state = STATE_SYNC;
}

void RingNet::begin(uint32_t baud)
{
  serial->begin(baud, RINGNET_SERIAL_CONFIG);
}

void RingNet::setAddress(const uint8_t address[ADDRESS_LENGTH])
{
  memcpy(localLongAddress, address, ADDRESS_LENGTH);
  if ((address[0] | address[1] | address[2] | address[3] | address[4] | address[5] | address[6]) == 0 && address[7] == 1)
  {
    localAddress = ADDRESS_MASTER;
  }
}

void RingNet::setOnAddress(OnAddress addr)
{
  addrFn = addr;
}

void RingNet::setOnReceive(OnReceive recv)
{
  recvFn = recv;
}

void RingNet::setOnReady(OnReady ready)
{
  readyFn = ready;
}

void RingNet::setOnStatus(OnStatus status)
{
  statusFn = status;
}

void RingNet::setOnReset(OnReset reset)
{
  resetFn = reset;
}

void RingNet::setOnResolved(OnResolved resolved)
{
  resolvedFn = resolved;
}

void RingNet::setOnDebug(OnDebug debug)
{
  debugFn = debug;
}

void RingNet::setOnNetworkChange(OnNetworkChange change)
{
  changeFn = change;
}

uint8_t RingNet::nodeCount(void)
{
  return nrNodes;
}

void RingNet::loop(void)
{
  // If network has been quiet for a while, we may have a fault. Reset and send keepalive
  if ((uint16_t)(millis() - lastTime) > RINGNET_TIMEOUT)
  {
    // Reset the network state (it may already be in SYNC, but that's okay)
    state = STATE_SYNC;
    // Send a SYNC keepalive
    serial->write(PKT_SYNC);
    lastTime = millis();
  }

  // If we can send a new packet, have nothing pending to forward, and are in a state
  // where a new packet can be injected, let the program know.
  if (serial->availableForWrite() > 0 && !serial->available() && state <= STATE_DST)
  {
    ready();
  }
  while (serial->available() && serial->availableForWrite() > 0)
  {
    lastTime = (uint16_t)millis();
    uint8_t data = serial->read();
    switch (state)
    {
      case STATE_SYNC:
        if (data == PKT_SYNC)
        {
          ready();
          state = STATE_DST;
          serial->write(PKT_SYNC);
        }
        else
        {
          // Don't pass on data when we're out-of-sync
        }
        break;
      case STATE_DST:
        {
          uint8_t dstAddress = data & PKT_DST_MASK;
          if (data == PKT_SYNC)
          {
            // We just forwarded a sync, so we don't forward this one and wait for the src again
          }
          else if ((data & PKT_DST_MASTERED) != 0 && localAddress == ADDRESS_MASTER)
          {
            // If this is the master node, and this packet has been here before, we ditch it so
            // it doesn't circulate forever.
            state = STATE_SYNC;
            error(ERROR_REMOVED);
            ready();
            serial->write(PKT_SYNC);
          }
          else if (dstAddress == localAddress)
          {
            // Packet for us
            state = STATE_SRC;
            forwardAddress = dstAddress;
          }
          else if (dstAddress == ADDRESS_NEXT)
          {
            // Next address - always for us
            state = STATE_SRC;
            forwardAddress = dstAddress;
          }
          else
          {
            // Not for us - forward
            forwardAddress = dstAddress;
            state = STATE_SRCFORWARD;
            serial->write(data | (localAddress == ADDRESS_MASTER ? PKT_DST_MASTERED : 0));
          }
        }
        break;
      case STATE_SRC:
        if (data == PKT_SYNC)
        {
          // Discard pkt so far and resync
          state = STATE_SYNC;
          ready();
          state = STATE_DST;
          serial->write(PKT_SYNC);
        }
        else
        {
          state = STATE_PROTOCOL;
          remoteAddress = data;
        }
        break;
      case STATE_PROTOCOL:
        state = STATE_DATA;
        bufferPtr = buffer;
        remoteProtocol = data;
        break;
      case STATE_DATA:
        if (data == PKT_SYNC)
        {
          // End of packet
          state = STATE_SYNC;
          // Packet needs at least 2 bytes (data + crc)
          if (bufferPtr >= buffer + 2)
          {
            // Check crc
            uint8_t crc = 0;
            CRC8(crc, forwardAddress);
            CRC8(crc, remoteAddress);
            CRC8(crc, remoteProtocol);
            bufferPtr--;
            for (uint8_t* src = buffer; src < bufferPtr; src++)
            {
              CRC8(crc, *src);
            }
            if (crc != bufferPtr[0])
            {
              // CRC failed - error
              // Send nacks for all direct messages
              if ((remoteProtocol & PROTOCOL_ACK_BIT) != 0)
              {
                const uint8_t nack[] = { COMMAND_NACK };
                sendInternal(remoteAddress, localAddress, PROTOCOL_SYSTEM, nack, sizeof(nack), false);
              }
              error(ERROR_CRC);
            }
            else
            {
              // Packet okay - deliver
              recv();
            }
          }
          else
          {
            // Empty packet - error
            error(ERROR_EMPTY);
          }
          ready();
          state = STATE_DST;
          serial->write(PKT_SYNC);
        }
        else
        {
          if (bufferPtr >= buffer + RINGNET_MAX_DATA)
          {
            // Too big - discard data and write a sync so upstream
            // will know the packet has ended (but crc will fail)
            error(ERROR_TOOBIG);
            state = STATE_SYNC;
            ready();
          }
          else
          {
            if (data == PKT_ESC)
            {
              state = STATE_ESCDATA;
            }
            else
            {
              *bufferPtr++ = data;
            }
          }
        }
        break;
      case STATE_ESCDATA:
        if (data > 0x7F)
        {
          state = STATE_SYNC;
          error(ERROR_BADDATA);
          ready();
          // If data looks like a new sync, treat it as a new sync
          if (data == PKT_SYNC)
          {
            state = STATE_DST;
          }
          serial->write(PKT_SYNC);
        }
        else
        {
          state = STATE_DATA;
          *bufferPtr++ = 0x80 | data;
        }
        break;

      case STATE_SRCFORWARD:
        if (data == PKT_SYNC)
        {
          state = STATE_SYNC;
          ready();
          state = STATE_DST;
          serial->write(PKT_SYNC);
        }
        else
        {
          state = STATE_DATAFORWARD;
          serial->write(data);
        }
        break;

      case STATE_DATAFORWARD:
        if (data == PKT_SYNC)
        {
          state = STATE_SYNC;
          ready();
          state = STATE_DST;
        }
        serial->write(data);
        break;
      
      default:
        break;
    }
  }
}

void RingNet::recv(void)
{
  uint8_t len = bufferPtr - buffer;
  switch (remoteProtocol & PROTOCOL_LEVEL_BIT)
  {
    case PROTOCOL_SYSTEM:
      switch (buffer[0])
      {
        case COMMAND_RESET:
          // If command was send from here, this is it returning home. Dont forward.
          if (remoteAddress == localAddress)
          {
            // Reset complete - now confirm
            loopTime = 2 * (millis() - pending.time);
            nrNodes = buffer[1] - ADDRESS_FIRST;
            const uint8_t cmd[] = { COMMAND_RESET_CONFIRM, nrNodes, loopTime >> 8, loopTime & 0xFF };
            sendInternalRetry(localAddress, ADDRESS_NEXT, PROTOCOL_SYSTEM, cmd, sizeof(cmd), false);
          }
          else if (len == 2)
          {
            localAddress = buffer[1]++;
            sendInternalRetry(remoteAddress, ADDRESS_NEXT, remoteProtocol, buffer, len, false);
          }
          break;

        case COMMAND_RESET_CONFIRM:
          if (remoteAddress == localAddress)
          {
            flags = flags & ~FLAG_IN_RESET;
            if (resetFn)
            {
              resetFn();
            }
          }
          else if (len == 4)
          {
            nrNodes = buffer[1];
            loopTime = ((uint16_t)buffer[2] << 8) | buffer[3];
            sendInternalRetry(remoteAddress, ADDRESS_NEXT, remoteProtocol, buffer, len, false);
          }
          break;

        case COMMAND_RESOLVE:
          if (remoteAddress == localAddress)
          {
            // Unresolved
            if (resolvedFn)
            {
              resolvedFn(ADDRESS_NULL, &buffer[1]);
            }
          }
          else if (len == 9)
          {
            if (memcmp(localLongAddress, &buffer[1], 8) == 0)
            {
              // My address - resolved
              buffer[0] = COMMAND_RESOLVED;
              sendInternalRetry(localAddress, remoteAddress, remoteProtocol, buffer, len, false);
            }
            else
            {
              sendInternalRetry(remoteAddress, ADDRESS_NEXT, remoteProtocol, buffer, len, false);
            }
          }
          break;

        case COMMAND_RESOLVED:
          if (resolvedFn)
          {
            resolvedFn(remoteAddress, &buffer[1]);
          }
          break;

        case COMMAND_LIST:
          if (remoteAddress != localAddress)
          {
            const uint8_t cmd[] = { COMMAND_RESOLVED, localLongAddress[0], localLongAddress[1], localLongAddress[2], localLongAddress[3], localLongAddress[4], localLongAddress[5], localLongAddress[6], localLongAddress[7] };
            sendInternalRetry(remoteAddress, ADDRESS_NEXT, remoteProtocol, buffer, len, false);
            sendInternalRetry(localAddress, remoteAddress, PROTOCOL_SYSTEM, cmd, sizeof(cmd), false);
          }
          break;

        case COMMAND_SET_ADDRESS:
          if (len == ADDRESS_LENGTH + 1)
          {
            if (addrFn)
            {
              addrFn(&buffer[1]);
            }
          }
          break;

        case COMMAND_NETWORK_CHANGE:
          if (changeFn)
          {
            changeFn(remoteAddress);
          }
          break;

        case COMMAND_DEBUG_ENABLE:
          if (len == 2)
          {
            flags = (flags & ~FLAG_DEBUG_ENABLED) | (buffer[1] ? FLAG_DEBUG_ENABLED : 0);
          }
          break;

        case COMMAND_DEBUG:
          // Debug commands are dispatched in the usual way to the master only.
          if (localAddress == ADDRESS_MASTER && debugFn)
          {
            char buf[len + 1];
            memcpy(buf, buffer, len);
            buf[len] = 0;
            debugFn(remoteAddress, buf);
          }
          break;

        case COMMAND_ACK:
          ack(true);
          break;

        case COMMAND_NACK:
          ack(false);
          break;

        default:
          // Forward command, even if we don't understand it
          sendInternalRetry(remoteAddress, ADDRESS_NEXT, remoteProtocol, buffer, len, false);
          break;
      }
      break;

    case PROTOCOL_USER:
      // Send acks as necessary
      if ((remoteProtocol & PROTOCOL_ACK_BIT) != 0)
      {
        const uint8_t ack[] = { COMMAND_ACK };
        sendInternalRetry(localAddress, remoteAddress, PROTOCOL_SYSTEM, ack, sizeof(ack), false);
      }
      if (recvFn)
      {
        recvFn(remoteAddress, buffer, len);
      }
      break;

    default:
      break;
  }
  remoteAddress = ADDRESS_NULL;
}

void RingNet::ack(bool okay)
{
  if (okay)
  {
    pending.data = NULL;
    error(ACK);
  }
  else
  {
    pending.time = millis();
    const uint8_t* data = pending.data;
    pending.data = NULL;
    sendInternal(localAddress, pending.address, pending.protocol, data, pending.length, true);
  }
}

void RingNet::ready(void)
{
  // Re-run reset if it's timed-out
  if ((flags & FLAG_IN_RESET) != 0 && (uint16_t)(millis() - pending.time) > RINGNET_RESET_TIMEOUT)
  {
    reset();
  }
  else
  {
    // Timeout retransmission
    if (pending.data != NULL && (uint16_t)(millis() - pending.time) > loopTime)
    {
      ack(false);
    }
    if (readyFn)
    {
      readyFn();
    }
  }
}

void RingNet::error(StatusCode err)
{
  if (statusFn)
  {
    statusFn(err);
  }
}

bool RingNet::available(void)
{
  return (state == STATE_SYNC || state == STATE_DST) && resetFn == NULL && localAddress != ADDRESS_NULL;
}

RingNet::StatusCode RingNet::reset(void)
{
  flags |= FLAG_IN_RESET;
  nrNodes = 0;
  pending.time = millis(); // Measure how long the full reset takes
  const uint8_t cmd[] = { COMMAND_RESET, ADDRESS_FIRST };
  return sendInternal(localAddress, ADDRESS_NEXT, PROTOCOL_SYSTEM, cmd, sizeof(cmd), false);
}

RingNet::StatusCode RingNet::reply(const uint8_t* data, const uint8_t length, const bool ack)
{
  return send(remoteAddress, data, length, ack);
}

RingNet::StatusCode RingNet::send(const uint8_t remoteAddress, const uint8_t* data, const uint8_t length, const bool ack)
{
  if (localAddress == ADDRESS_NULL)
  {
    return ERROR_INITIALIZING;
  }
  else if (remoteAddress == ADDRESS_NULL)
  {
    return ERROR_ADDRESS;
  }
  else
  {
    return sendInternal(localAddress, remoteAddress, PROTOCOL_USER, data, length, ack);
  }
}

RingNet::StatusCode RingNet::resolve(const uint8_t longAddress[8])
{
  if (localAddress == ADDRESS_NULL)
  {
    return ERROR_INITIALIZING;
  }
  else
  {
    const uint8_t cmd[] = { COMMAND_RESOLVE, longAddress[0], longAddress[1], longAddress[2], longAddress[3], longAddress[4], longAddress[5], longAddress[6], longAddress[7] };
    return sendInternal(localAddress, ADDRESS_NEXT, PROTOCOL_SYSTEM, cmd, sizeof(cmd), false);
  }
}

RingNet::StatusCode RingNet::list(void)
{
  if (localAddress == ADDRESS_NULL)
  {
    return ERROR_INITIALIZING;
  }
  else
  {
    const uint8_t cmd[] = { COMMAND_LIST };
    return sendInternal(localAddress, ADDRESS_NEXT, PROTOCOL_SYSTEM, cmd, sizeof(cmd), false);
  }
}

RingNet::StatusCode RingNet::networkChange(void)
{
  if (localAddress == ADDRESS_NULL)
  {
    return ERROR_INITIALIZING;
  }
  else if (localAddress == ADDRESS_MASTER)
  {
    if (changeFn)
    {
      changeFn(localAddress);
    }
    return OK;
  }
  else
  {
    const uint8_t cmd[] = { COMMAND_NETWORK_CHANGE };
    return sendInternal(localAddress, ADDRESS_MASTER, PROTOCOL_SYSTEM, cmd, sizeof(cmd), false);
  }
}

RingNet::StatusCode RingNet::sendInternalRetry(const uint8_t localAddress, const uint8_t remoteAddress, uint8_t protocol, const uint8_t* data, const uint8_t length, const bool ack)
{
  
  for (;;)
  {
    StatusCode code = sendInternal(localAddress, remoteAddress, protocol, data, length, ack);
    if (code == ERROR_BUSY)
    {
      delayMicroseconds(100);
    }
    else
    {
      return code;
    }
  }
}

RingNet::StatusCode RingNet::sendInternal(const uint8_t localAddress, const uint8_t remoteAddress, uint8_t protocol, const uint8_t* data, const uint8_t length, const bool ack)
{
  printf("SendInternal: local %d remote %d protocol %d len %d ack %d - ", localAddress, remoteAddress, protocol, length, ack);
  for (int i = 0; i < length; i++) printf("%d ", data[i]);
  printf("\n");

  if (localAddress == ADDRESS_NULL)
  {
    return ERROR_INITIALIZING;
  }
  if (state > STATE_DST)
  {
    return ERROR_BUSY;
  }
  if (length > RINGNET_MAX_DATA)
  {
    return ERROR_TOOBIG;
  }
  if (ack == true)
  {
    if (pending.data != NULL)
    {
      return ERROR_PENDING;
    }
    pending.time = millis();
    pending.address = remoteAddress;
    pending.protocol = protocol;
    pending.data = data;
    pending.length = length;
    protocol |= PROTOCOL_ACK_BIT;
  }
  uint8_t crc = 0;
  CRC8(crc, remoteAddress);
  CRC8(crc, localAddress);
  CRC8(crc, protocol);

  // Calculate actual size of data (including overhead). We can only send if this will
  // fit into the tx buffer.
  uint8_t alen = length + 5;
  for (uint8_t i = 0; i < length; i++)
  {
    uint8_t v = data[i];
    CRC8(crc, v);
    if (v == PKT_SYNC || v == PKT_ESC)
    {
      alen++;
    }
  }
  if (crc == PKT_SYNC || crc == PKT_ESC)
  {
    alen++;
  }
  if (alen > serial->availableForWrite())
  {
    if (ack == true)
    {
      pending.data = NULL;
    }
    return ERROR_BUSY;
  }

  serial->write(PKT_SYNC);
  // When sending a reset message we add some extra sync bytes. This helps to clear up any errors
  // and also deal with recently started devices having serial noise to deal with.
  if (protocol == PROTOCOL_SYSTEM && data[0] == COMMAND_RESET)
  {
    for (uint8_t i = 0; i < 7; i++)
    {
      serial->write(PKT_SYNC);
    }
  }
  serial->write(remoteAddress);
  serial->write(localAddress);
  serial->write(protocol);
  for (uint8_t i = 0; i < length; i++)
  {
    uint8_t v = data[i];
    if (v == PKT_SYNC || v == PKT_ESC)
    {
      serial->write(PKT_ESC);
      v &= 0x7F;
    }
    serial->write(v);
  }
  if (crc == PKT_SYNC || crc == PKT_ESC)
  {
    serial->write(PKT_ESC);
    crc &= 0x7F;
  }
  serial->write(crc);
  serial->write(PKT_SYNC);

  return OK;
}

RingNet::StatusCode RingNet::enableDebug(const uint8_t remoteAddress, const bool enable)
{
  if (remoteAddress == ADDRESS_MASTER)
  {
    flags = (flags & ~FLAG_DEBUG_ENABLED) | (enable ? FLAG_DEBUG_ENABLED : 0);
    return OK;
  }
  else
  {
    const uint8_t buf[] = { COMMAND_DEBUG_ENABLE, enable };
    return sendInternal(localAddress, remoteAddress, PROTOCOL_SYSTEM, buf, sizeof(buf), false);
  }
}

RingNet::DebugPrint::DebugPrint(RingNet* net)
{
  this->net = net;
}

size_t RingNet::DebugPrint::write(uint8_t ch)
{
  if ((net->flags & FLAG_DEBUG_ENABLED) == 0)
  {
    return 0;
  }
  else if (net->localAddress != ADDRESS_MASTER)
  {
    uint8_t buf[] = { COMMAND_DEBUG, ch };
    net->sendInternalRetry(net->localAddress, ADDRESS_MASTER, PROTOCOL_SYSTEM, buf, sizeof(buf), false);
    return 1;
  }
  else if (net->debugFn)
  {
    const char buf[] = { ch, 0 };
    net->debugFn(ADDRESS_MASTER, buf);
    return 1;
  }
  else
  {
    return 0;
  }
}

size_t RingNet::DebugPrint::write(const uint8_t* buffer, size_t len)
{
  if ((net->flags & FLAG_DEBUG_ENABLED) == 0)
  {
    return 0;
  }
  if (len + 1 > RINGNET_MAX_DATA)
  {
    len = RINGNET_MAX_DATA - 1;
  }
  if (net->localAddress == ADDRESS_MASTER)
  {
    if (net->debugFn)
    {
      uint8_t msg[len + 1];
      memcpy(msg, buffer, len);
      msg[len] = 0;
      net->debugFn(ADDRESS_MASTER, (const char*)msg);
      return len;
    }
    return 0;
  }
  else
  {
    uint8_t buf[len + 1];
    buf[0] = COMMAND_DEBUG;
    memcpy(&buf[1], buffer, len);
    net->sendInternalRetry(net->localAddress, ADDRESS_MASTER, PROTOCOL_SYSTEM, buf, sizeof(buf), false);
    return len;
  }
}

Print* RingNet::debug(void)
{
#if defined(SHIM)
  return new DebugPrint(this);
#else
  static DebugPrint debug(this);
  return &debug;
#endif
}
